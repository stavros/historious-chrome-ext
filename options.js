// Saves options to localStorage.
function save_options() {
  var apikey = document.getElementById("apikey").value;
  if (apikey == "")
      localStorage.removeItem("apikey");
  else
      localStorage.apikey = apikey;

  var bypass = document.getElementById("bypass").checked;
  if (!bypass || bypass == "false")
    localStorage.bypass = 0;
  else
    localStorage.bypass = 1;

  var prompt_tags = document.getElementById("prompt_tags").checked;
  if (!prompt_tags || prompt_tags == "false")
    localStorage.prompt_tags = 0;
  else
    localStorage.prompt_tags = 1;

  // Update status to let user know options were saved.
  var status = document.getElementById("status");
  status.innerHTML = "Options saved!";
  setTimeout(function() {
    status.innerHTML = "";
  }, 1000);
}

// Restores select box state to saved value from localStorage.
function restore_options() {
  var apikey = localStorage["apikey"];
  if (!apikey) {
    apikey = "";
  }

  var bypass = localStorage.bypass;
  if (!bypass || bypass == 1)
    bypass = true;
  else
    bypass = false;

  var prompt_tags = localStorage.prompt_tags;
  if (!prompt_tags || prompt_tags == 0)
    prompt_tags = false;
  else
    prompt_tags = true;

  document.getElementById("apikey").value = apikey;
  document.getElementById("bypass").checked = bypass;
  document.getElementById("prompt_tags").checked = prompt_tags;
}

window.addEventListener("DOMContentLoaded", () => {

  restore_options();

  document.querySelector('button').addEventListener('click', () => {
    save_options();
  });
});
