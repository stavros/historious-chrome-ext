var agent = 'historious Chrome extension 1.3.3';
var processingCount = 0;
var canvas, ctx, icon;
var animationFrames = 8;
var animationSpeed = 150; // ms
var rotation = 0;
var animating = false;

chrome.browserAction.onClicked.addListener(function (tab) {

    startProcessing();

    chrome.tabs.executeScript(null, {
      file: "page.js",
      runAt: "document_end"
    }, (response) => {

        if (chrome.runtime.lastError) {

            stopProcessing();
            return;
        }

        chrome.tabs.sendMessage(tab.id, {
            'type': 'get_source',
            'tags': parseInt(localStorage.prompt_tags),
            'id': tab.id,
            'title': tab.title,
            'url': tab.url
        }, (response) => {

            if(chrome.runtime.lastError) {

                stopProcessing();
            }
        });
    });

});

function init() {
    icon = document.createElement('img');
    icon.src = "icon16.png";
    icon.onload = () => {

    }
    canvas = document.createElement('canvas');
    canvas.width = 16;
    canvas.height = 16;

    ctx = canvas.getContext('2d');
    chrome.browserAction.setBadgeBackgroundColor({color: [208, 0, 24, 255]});
}

function swirl() {
    if (processingCount < 1) {
        rotation = 0;
        chrome.browserAction.setIcon({path: 'icon19.png'});
        chrome.browserAction.setBadgeText({text: ""});
        return;
    }

    rotation += 1/animationFrames;
    iconRotate();

    if (rotation >= 1)
        rotation = 0;

    setTimeout(() => { swirl() }, animationSpeed);
}

function ease(x) {
  return (1 - Math.sin(Math.PI/2 + x*Math.PI)) / 2;
}

function iconRotate() {
    ctx.save();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.translate(Math.ceil(canvas.width/2),
                  Math.ceil(canvas.height/2));
    ctx.rotate(2 * Math.PI * ease(rotation));
    ctx.drawImage(icon,
                  -Math.ceil(canvas.width/2),
                  -Math.ceil(canvas.height/2));
    ctx.restore();

    chrome.browserAction.setIcon({
        imageData: ctx.getImageData(0, 0, canvas.width, canvas.height)
    });
}

function startProcessing() {
    processingCount++;
    chrome.browserAction.setBadgeText({text: processingCount.toString()});
    if (processingCount === 1) {
        swirl();
    }
}

function stopProcessing() {
    processingCount--;

    if(processingCount < 0) {
        processingCount = 0;
    }

    chrome.browserAction.setBadgeText({text: processingCount.toString()});
}

function makeFormData(data) {
    var formdata = "";
    for (var key in data) {
        formdata = formdata + "&" + key + "=" + encodeURIComponent(data[key]);
    }
    return formdata.slice(1);
}

function xhr(url, data, onsuccess, onerror) {
    var xhrequest = new XMLHttpRequest();

    xhrequest.onreadystatechange = function () {
        if (xhrequest.readyState != 4)
            return;

        if (xhrequest.responseText)
            onsuccess(xhrequest.responseText);
    }

    xhrequest.onerror = function (error_param) {
        onerror(error_param);
    }

    xhrequest.open('POST', url, true);
    xhrequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhrequest.send(makeFormData(data));
}

function save(key, url, title, content, tags) {
    var destination = 'https://historio.us/api/add/?key=' + key;
    var contentdict = {'agent': agent, 'url': url};

    if (typeof localStorage.bypass == 'undefined' ||
        localStorage.bypass == 1) {

        contentdict["source"] = content;
    }

    if (typeof tags == 'string' && tags.length > 0) {

        contentdict["tags"] = tags;
    }

    xhr(destination, contentdict,
        function (response) {
            var code = JSON.parse(response);
            stopProcessing();
            switch (code) {
            case 0:
                // success
                break;
            case 1:
                // already saved
                report("This page was already in your historious.")
                break;
            case 2:
                error('Your key is invalid. Please sign in to historious again so we can retrieve it.');
                localStorage.removeItem("apikey");
                chrome.tabs.create({url: 'http://historio.us/login/'});
                break;
            case 3:
                error('Invalid request or site url');
                break;
            case 4:
                error('You have reached the limit of historified sites. Please upgrade your account!');
                break;
            default:
                error('Code ' + code);
                break;
            }
        },
        error);
}

function getkey(callback) {
    if (localStorage.apikey)
        return callback(localStorage.apikey);

    xhr('https://historio.us/api/getkey/', {'agent': agent},
        function (response) {
            response = JSON.parse(response);

            if (typeof response === 'number') { //error
                if (response === 2) {
                    report('We cannot historify this page, as you need to sign in to historious to complete the installation. Please retry after signing in!');
                    chrome.tabs.create({url: 'http://historio.us/login/'});
                } else {
                    error('Code ' + response);
                }
                stopProcessing();

            } else {
                localStorage.apikey = response;
                return callback(response);
            }
        },
        error);
}

function report(message) {

    chrome.notifications.create({
        'type'              : 'basic',
        'title'             : 'historious',
        'message'           : message,
        'iconUrl'           : 'icon48.png',
    }, (notificationId) => {  });
}

function error(message) {
    report(message);
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {

    if(typeof request.html == 'undefined')
        return;

    var title = request.title;
    var url = request.url;
    var content = request.html;
    var tags = request.tags;

    getkey(function (key) {
        save(key, url, title, content, tags);
    });

    sendResponse();
});

init();
