if(!window.historifyExtension) {

	(function() {

      var HistorifyExtension = {

        historiousSendSource : function(request) {

			var source = document.getElementsByTagName('html')[0].outerHTML;

			var tags = "";

			if(request.tags == 1) 
				tags = prompt("Please enter some tags for this webpage:", "");

			chrome.runtime.sendMessage({ 
			    'html': source,
				'id': request.id,
				'title': request.title,
				'url': request.url,
				'tags': tags
			});	
		}
      };

      window.historifyExtension = HistorifyExtension;

    })(window);

	chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {

		if(request.type != "get_source")
			return;

	    if (document.readyState != "complete") {

	        window.onload = () => {
	        	historifyExtension.historiousSendSource(request);
	        };

	    } else {

	    	historifyExtension.historiousSendSource(request);
	    }

	    sendResponse();
	});
}
